import xmltodict
from django.http import JsonResponse
from django.shortcuts import render


def _post_processor(path, key, value):
    """Post processor that checks if the value of an given key is not provided
    and set it to an empty string
    """
    if not value:
        return key, ""
    return key, value


def upload_page(request):
    """Handles requests to the root `/` endpoint
    """
    if request.method == 'POST':
        file = request.FILES['file']

        # Read the file and convert to string
        data = file.read()
        xml_str = data.decode('utf-8')

        # Parse the xml string to a dict
        dict = xmltodict.parse(xml_str, postprocessor=_post_processor)

        # To confirm data can be passed back to xml correctly
        # print(xmltodict.unparse(dict, pretty=True))

        # Return the dictionary as a json
        return JsonResponse(dict)

    return render(request, "upload_page.html")
